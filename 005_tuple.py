a, b  = (1, 2)

print(a)
print(b)


"""
a, b, c  = (1, 2)
# ValueError: not enough values to unpack (expected 3, got 2)
print(a)
print(b)
"""


"""
a, b = (1, 2, 3)
# a, b, c  = (1, 2)
# ValueError: not enough values to unpack (expected 3, got 2)
print(a)
print(b)
"""

print("\n----------------\n")
a, b, *_ = (1, 2, 3, 4)
print(a)
print(b)
print(_)

print("\n-----------------\n")


a, b, *_, d = (1, 2, 3, 4, 5, 6, 7, 8)
print(a)
print(b)
print(_)
print(d)

print("\n-----------------\n")

a, b, *_, d = (1, 2, 3, 4, 5, 6, 7, 8)
print(a)
print(b)
print(*_)
print(d)