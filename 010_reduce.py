"""
product = 1
list = [1, 2, 3, 4]
for num in list:
    product = product * num
"""
# همانند تابع فیلتر است 
# با این تفاوت که خروجی آن یک مجموعه نیست که به توان آن را لیست کرد 
# و خروجی آن یک عدد می باشد
from functools import reduce
product = reduce((lambda x, y: x * y), [1, 2, 3, 4])
print(product)