"""
f = open("test.txt", "r") # <----

file_contents = f.read()

f.close() # <----

words = file_contents.split(" ")
words_count = len(words)
print(words_count)

"""

with open("test.txt", "r") as f: # <----
    file_contents = f.read()


words = file_contents.split(" ")
words_count = len(words)
print(words_count)