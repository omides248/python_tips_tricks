"""

names = ["Omid", "Ali", "Amin", "Hamed"]

index = 0
for name in names:
    print(index, name)
    index += 1

"""

names = ["Omid", "Ali", "Amin", "Hamed"]
cars = ["genesis", "maxima", "bugatti", "karma"]
colors = ["golden", "blue", "white", "black"]

for index, name in enumerate(names, start=1):
    print(index, name)


"""
for index, name in enumerate(names):
    car = cars[index]
    print(f"{name} has a {car} car.")

"""

for name, car, color in zip(names, cars, colors):
    print(f"{name} has a {color} {car} car.")