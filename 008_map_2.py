def multiply(x):
    return (x*x)

def add(x):
    return (x+x)


funcs = [multiply, add]

for i in range(5):
    value = list(map(lambda x: x(i), funcs))
    
    # [ multiply(0), add(0)] -> [0, 0]
    # [ multiply(1), add(1)] -> [1, 2]
    # [ multiply(2), add(2)] -> [4, 4]
    # [ multiply(3), add(3)] -> [9, 6]
    # [ multiply(4), add(4)] -> [16, 8]
    print(value)